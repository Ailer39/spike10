#include "Inventory.h"

list<GameObject*>* inventory;

void Inventory::Print()
{
	cout << endl;
	for (list<GameObject*>::iterator i = inventory->begin(); i != inventory->end(); i++)
	{
		cout << "Item: " << (*i)->GetName();
	}
}

void Inventory::Add(GameObject* item)
{
	inventory->insert(inventory->begin(), item);
}

GameObject* Inventory::UseItem(string itemName)
{
	GameObject* tmp = nullptr;
	for (list<GameObject*>::iterator i = inventory->begin(); i != inventory->end(); i++)
	{
		if ((*i)->GetName() == itemName)
		{
			tmp = (*i);
			break;
		}
	}

	return tmp;
}

void Inventory::RemoveItem(string itemName)
{
	GameObject* tmp = nullptr;
	for (list<GameObject*>::iterator i = inventory->begin(); i != inventory->end(); i++)
	{
		if ((*i)->GetName() == itemName)
		{
			tmp = (*i);
			break;
		}
	}

	inventory->remove(tmp);
}

Inventory::Inventory()
{
	inventory = new list<GameObject*>();
}

Inventory::~Inventory()
{
	delete inventory;
}