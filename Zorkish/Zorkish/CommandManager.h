#pragma once
#include <string>
#include <regex>
#include <map>
#include "MoveCommand.h"
#include "LookCommand.h"
#include "LookAtCommand.h"
#include "LookInCommand.h"
#include "AttackCommand.h"

using namespace std;
using namespace std::regex_constants;

class CommandManager
{
private:
	Player* _player;
	map<string, BaseCommand*> _commands;
public:
	void ExecuteCommand(string command);
	CommandManager(Player* player);
};