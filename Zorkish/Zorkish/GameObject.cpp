#include "GameObject.h"
#include <boost/algorithm/string.hpp>

GameObject::GameObject(string name, string description)
{
	_name = name;
	_description = description;
}

string GameObject::GetName()
{
	return _name;
}

void GameObject::SetName(string name)
{
	_name = name;
}

void GameObject::AddGameObject(GameObject* gameObj)
{
	_gameObjects.insert(_gameObjects.begin(), gameObj);
}

void GameObject::RemoveGameObject(string name)
{
	vector<GameObject*>::iterator foundElement;
	for (vector<GameObject*>::iterator i = _gameObjects.begin(); i != _gameObjects.end(); i++)
	{
		if (boost::iequals(name, (*i)->GetName()))
		{
			foundElement = i;
			break;
		}
	}

	_gameObjects.erase(foundElement);
}

GameObject* GameObject::GetGameObject(string name)
{
	vector<GameObject*>::iterator foundElement;
	for (vector<GameObject*>::iterator i = _gameObjects.begin(); i != _gameObjects.end(); i++)
	{
		if (boost::iequals(name, (*i)->GetName()))
		{
			return (*i);
		}
	}

	return nullptr;
}

string GameObject::GetDescription()
{
	return _description;
}

void GameObject::LookAt()
{
	cout << GetDescription() << endl;
}

void GameObject::LookIn()
{
	if (_gameObjects.size() > 0)
	{
		cout << "You see a: " << endl;
		for (auto child : _gameObjects)
		{
			cout << child->GetName() << endl;
		}
	}
	else
	{
		cout << "Their is nothing inside" << endl;
	}
}

vector<GameObject*> GameObject::GetChilds()
{
	return _gameObjects;
}

void GameObject::AddComponent(string componentName, ComponentBase* component)
{
	if (_components.find(componentName) == _components.end())
	{
		_components.insert(_components.end(), pair<string, ComponentBase*>(componentName, component));
	}
}

void GameObject::RemoveComponent(string componentName)
{
	if (_components.find(componentName) != _components.end())
	{
		_components.erase(componentName);
	}
}

ComponentBase* GameObject::GetComponent(string componentName)
{
	if (_components.find(componentName) != _components.end())
	{
		return _components.at(componentName);
	}
	else
	{
		return nullptr;
	}
}