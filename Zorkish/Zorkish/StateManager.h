#pragma once

#include "StateBase.h"
#include "Location.h"

class StateManager
{
private:
	int _currentStateIndex;
public:
	StateManager();
	~StateManager() {};
	void ChangeState(int state);
	void ChangeToGameState(Location* level);
};