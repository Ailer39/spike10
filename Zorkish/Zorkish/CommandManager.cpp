#include "CommandManager.h"


void CommandManager::ExecuteCommand(string command)
{
	BaseCommand* cmd = nullptr;

	for (map<string, BaseCommand*>::iterator i = _commands.begin(); i != _commands.end(); i++)
	{
		if (regex_match(command, regex(i->first, ECMAScript | icase)))
		{
			cmd = i->second;
			break;
		}
	}

	if (cmd != nullptr)
	{
		cmd->execute(_player->GetCurrentPosition(), command);
	}
}

CommandManager::CommandManager(Player* player)
{
	_player = player;
	_commands.insert(pair<string, BaseCommand*>("^[ ]{0,}(go)?[ ]*(n|north)$",
		new MoveCommand(_player, Direction::North)));
	_commands.insert(pair<string, BaseCommand*>("^[ ]{0,}(go)?[ ]*(e|east)$",
		new MoveCommand(_player, Direction::East)));
	_commands.insert(pair<string, BaseCommand*>("^[ ]{0,}(go)?[ ]*(w|west)$",
		new MoveCommand(_player, Direction::West)));
	_commands.insert(pair<string, BaseCommand*>("^[ ]{0,}(go)?[ ]*(s|south)$",
		new MoveCommand(_player, Direction::South)));
	_commands.insert(pair<string, BaseCommand*>("^(look)$",
		new LookCommand()));
	_commands.insert(pair<string, BaseCommand*>("^look at[ ]+[a-z]*$",
		new LookAtCommand()));
	_commands.insert(pair<string, BaseCommand*>("^look in[ ]+[a-z]*$",
		new LookInCommand()));
	_commands.insert(pair<string, BaseCommand*>("^attack[ ]+[a-z]*$",
		new AttackCommand()));
}