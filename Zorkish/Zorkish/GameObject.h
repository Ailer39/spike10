#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <map>
#include "ComponentBase.h"

using namespace std;

class GameObject
{
private:
	vector<GameObject*> _gameObjects;
	map<string, ComponentBase*> _components;
protected:
	GameObject() {};
	void SetName(string name);
	string _name;
	string _description;
	vector<GameObject*> GetChilds();
public:
	GameObject(string name, string description);
	string GetName();	
	string GetDescription();

	// Child game objects
	void AddGameObject(GameObject* gameObj);
	void RemoveGameObject(string name);
	GameObject* GetGameObject(string name);

	// Components
	void AddComponent(string componentName, ComponentBase* component);
	void RemoveComponent(string componentName);
	ComponentBase* GetComponent(string componentName);

	// Commands
	void LookIn();
	void LookAt();
};