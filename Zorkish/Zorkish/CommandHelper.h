#pragma once
#include <string>
#include "GameObject.h"

using namespace std;

class CommandHelper
{
public:
	static string GetGameObjectNameFromCommand(string command, string key);
};