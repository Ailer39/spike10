#pragma once
#pragma once
#include "BaseCommand.h"
#include "Location.h"
#include "Player.h"
#include "HealthComponent.h"
#include "CommandHelper.h"

class AttackCommand :
	public BaseCommand
{
public:

	void execute(Location* currentLocation = nullptr, string cmdText = "")
	{
		string objName = CommandHelper::GetGameObjectNameFromCommand(cmdText, "attack");
		GameObject* gObj = currentLocation->GetGameObject(objName);

		if (gObj != nullptr)
		{
			HealthComponent* health = ((HealthComponent*)gObj->GetComponent(HealthComponent::type));

			if (health != nullptr)
			{
				health->ChangeHealth(-50);
				health->PrintHealth();

				if (health->GetHealth() <= 0)
				{
					cout << "You killed " << gObj->GetName() << endl;
					currentLocation->RemoveGameObject(gObj->GetName());
					delete gObj;
				}
			}
			else
			{
				cout << "You can not attack " << gObj->GetName() << endl;
			}
		}
	}
};