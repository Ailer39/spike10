#pragma once
#include "BaseCommand.h"
#include "Location.h"
#include "Player.h"

class LookCommand :
	public BaseCommand
{
public:

	void execute(Location* currentLocation = nullptr, string cmdText = "")
	{
		currentLocation->Look();
	}
};