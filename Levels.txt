{
	"Locations" : 
	[
		{"Id" : 1, 
		"Name" : "House",
		"Description" : "This is the first location", 
		"Objects" : [
		{
			"Name": "Backpack",
			"Description" : "It is a old backpack",
			"Objects" : [
				{
					"Name" : "Flashlight",
					"Description" : "It is a flashlight.",
					"Objects" :[]
				}
			]
		},
		{
			"Name": "SleepingGuard",
			"Description" : "He is not your friend",
			"Live" : 100,
			"Objects" : []
		}],
		"ConnectedLocations" :[2]},
		{"Id" : 2, 		
		"Name" : "Forest",
		"Description" : "This is the second location",
		"Objects" : [],
		"ConnectedLocations" : [-1,1]}
	]
}