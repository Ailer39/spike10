#pragma once
#include "BaseCommand.h"
#include "Player.h"

class MoveCommand :
	public BaseCommand
{
private:
	Player* _player;
	Direction _moveDirection;
public:
	MoveCommand(Player* player, Direction direction)
	{
		_player = player;
		_moveDirection = direction;
	}

	void execute(Location* currentLocation = nullptr, string cmdText = "")
	{
		_player->MovePlayer(_moveDirection);
	}
};