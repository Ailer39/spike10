#include "Highscore.h"

static Highscore* instance;

Highscore* Highscore::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Highscore();
	}

	return instance;
}

void Highscore::Show(StateManager* context)
{
	cout << "Zorkish:Hall of fame" << endl << endl;
	cout << "1. Nils 999" << endl;
	cout << "2. ABC  455" << endl;
	cout << "3. XYZ  865" << endl;

	StateBase::Show(context);
}