#include "About.h"


static About* instance;

About* About::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new About();
	}

	return instance;
}

void About::Show(StateManager* context)
{
	cout << "Zorkish :: About" << endl;
	cout << "Written by: Nils Kunkel" << endl;
	StateBase::Show(context);
}